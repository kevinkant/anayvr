using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConversationUI : MonoBehaviour
{
    // Variables
    public GameObject conversationUI; //un objeto genérico que nos permitirá trabajr con todos los componentes visuales.
    public ServicioConversacion servicioConversacion; 

    // Use this for initialization
    void Start()
    {
        // ConversationService = new ConversationService ();
    }

    // Update is called once per frame
    void Update()
    {
        // Update code
        //esta es la parte donde se va mostrar y ocultar el componente visual dependiendo si el usuario quiere o no mandar comandos de voz. 
        if(Input.GetButtonDown("Conversation")) //estamos escanendo si el usuario a activado el comando. en nuetro caso se activa pulsando la tecla C.
        {
            bool flag = !conversationUI.activeSelf;
            conversationUI.SetActive(flag);
            if (flag)
            {
                servicioConversacion.iniciograbacion(); 
            }
            else
            {
                servicioConversacion.detenergrabacion();
            }
        }
    }
}